<?php
/**
 * Ant test for Ordnance Survey 
 * Author: Amit Aswani
 * Date: 10/03/2013
 * EMail: amit4got@gmail.com 
 */
class Ant {

	const MAX_HEALTH = 100;
	const SOLDIER_DEAD = 66;
	const QUEEN_DEAD = 20;
	const WORKER_DEAD = 50;
	const SOLDIER_ANT = 'SOLDIER';
	const QUEEN_ANT = 'QUEEN';
	const WORKER_ANT = 'WORKER';

	protected $_type;
	protected $_health;
	protected $_dead;

	/**
	 *
	 * @param string $type 
	 */
	public function __construct($type){
		if(in_array($type, array(self::SOLDIER_ANT, self::QUEEN_ANT, self::WORKER_ANT))) {
			$this->setType($type);
		}else{
			echo 'Type is incorrect';
		}
		$this->setHealth((float) self::MAX_HEALTH);
		$this->setDead(false);
	}

	public function setType($type = ''){
		$this->_type = $type;
	}

	public function setHealth($health = ''){
		$this->_health = $health;
	}

	public function setDead($dead = ''){
		$this->_dead = $dead;
	}

	public function getType(){
		return $this->_type;
	}

	public function getHealth(){
		return $this->_health;
	}

	public function getDead(){
		return $this->_dead;
	}

	/**
	 *
	 * @param string $asStr
	 * @return bool 
	 */
	public function isDead($asStr = false){
		if($asStr) return $this->getDead() ? 'Dead' : 'Alive';

		return (int)$this->getDead();
	}

	/**
	 * Set dead = true if health is below minimum ant type health
	 */
	public function killed(){
		switch ($this->getType()){
			case self::SOLDIER_ANT:
				if($this->getHealth() < $this->fromPercentageToDamage(self::SOLDIER_DEAD, self::MAX_HEALTH)) $this->setDead(true);
				break;
			case self::QUEEN_ANT:
				if($this->getHealth() < $this->fromPercentageToDamage(self::QUEEN_DEAD, self::MAX_HEALTH)) $this->setDead(true);
				break;
			case self::WORKER_ANT:
				if($this->getHealth() < $this->fromPercentageToDamage(self::WORKER_DEAD, self::MAX_HEALTH)) $this->setDead(true);
				break;
			default:
			break;
		}
	}

	/**
	 *
	 * @param int $percent
	 * @param int $health
	 * @return float precision 2
	 */
	public function fromPercentageToDamage($percent, $health = 0){
		if($health > 0) return round($health / 100 * $percent, 2);
		return round($this->getHealth() / 100 * $percent, 2);
	}

	/**
	 *
	 * @param int $damage 
	 */
	public function damage($damage){
		if(! $this->isDead()){
			$this->setHealth($this->getHealth() - $damage);
			$this->killed();
		}
	}

	public function formatted(){
		return $this->getType() . '|' . $this->getHealth() . '|' . $this->isDead();
	}

	/**
	 *
	 * @param string $type
	 * @param int $num
	 * @return array Ant $ants 
	 */
	public static function createAnt($type, $num = 1){
		if(! $type){
			echo 'Type is incorrect';
			die;
		}
		$ants = array();
		if($num==1) return new Ant($type);

		for($i=0;$i<10;$i++) $ants[] = new Ant($type);

		return $ants;
	}

}
