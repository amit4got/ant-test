<?php
/**
 *
 * Ant test for Ordnance Survey 
 * Author: Amit Aswani
 * Date: 10/03/2013
 * EMail: amit4got@gmail.com 
 */
include ('class/Ant.class.php');

if(isset($_POST['submitDamage'])){
	$ants = array();
	$ant_to_damage = explode(' ant ', $_POST['submitDamage']);
	for($i = 0; $i < 30; $i++){
		$ant = $_POST['ant'.$i];
		$data = explode('|', $ant);
		$ant = Ant::createAnt($data[0]);
		$ant->setHealth($data[1]);
		$ant->setDead($data[2]);
		//Damage all button clicked  ||   Damage Ant X button clicked
		if(! isset($ant_to_damage[1]) || $i == ($ant_to_damage[1] - 1)){
			$damage = $ant->fromPercentageToDamage(rand(0, 80));
			$ant->damage($damage);
		}
		$ants[] = $ant;
	}
}else{
	$ants = array_merge(Ant::createAnt(Ant::SOLDIER_ANT, 10), Ant::createAnt(Ant::QUEEN_ANT, 10), Ant::createAnt(Ant::WORKER_ANT, 10));
}
?>
<html>
<head>
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/style.css">
	<title>Ant test</title>
</head>
<body>
	<h1>Three types of ants (Soldiers, Queens and Workers)</h1>
	<h2>Click on each ant to place damage on them individually or click on the "Damage All" button to damage them all at the same time.</h2>
	<h3> Soldiers die when there health(H) is below 66%, Queens below 20% and Workers below 50%</h3>
	<h4>Between 0-80 percent of damage is produced to the ant/ants when the damage button is clicked.</h4>
	<div id="ants">
		<form method="post" action="index.php">
			<?php 
			$i = 0;
			$allDead = true;
			foreach($ants as $ant):
				$antType = $ant->getType();
				$antDead = $ant->isDead();
			?>
				<div class="
					<?php 
					if($antDead):?>
						colourDead
					<?php 
					else:
						switch($antType):
							case Ant::SOLDIER_ANT:
							?>
								colourSoldier
							<?php
							break;
							case Ant::QUEEN_ANT:
							?>
								colourQueen
							<?php
							break;
							case Ant::WORKER_ANT:
							?>
								colourWorker
							<?php
							break;
							default:
							break;
						endswitch;?>
					<?php 
					endif; ?>" >
					<div class="floatLeft ant <?php if($i % 2 == 0):?> marginRight20 clearBoth<?php endif;?>">
						<span class="antType"><?=$antType;?></span>
						<span class="antHealth">H:<?=$ant->getHealth();?></span>
						<span class="antStatus"><?=$ant->isDead(true);?></span>
						<?php if(! $ant->isDead()) $allDead = false; ?>
						<input class="submitWidth <?php if($ant->isDead()): ?>colourDead strike<?php endif; ?>" type="submit" name="submitDamage" value="Damage ant <?=($i + 1); ?>">
					</div>
					<input type="hidden" value="<?=$ant->formatted();?>" name="ant<?=$i; ?>">
				</div>
			<?php
				$i++;
			endforeach;
			?>
			<input class="clearBoth <?php if($allDead): ?>colourDead strike<?php endif; ?>" type="submit" name="submitDamage" value="Damage All">
			<input class="clearBoth" type="submit" name="reset" value="Reset">
		</form>
	</div>
</body>
</html>
